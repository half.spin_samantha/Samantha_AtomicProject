<?php


namespace App\Birthday;

use App\Message\Message;
use App\Model\Database;
use App\Utility\Utility;
use PDO;

class Birthday extends Database
{

    public $id,$user_id,$user_name,$date_of_birth;

    public function setData($postArray){

        if(array_key_exists("id",$postArray)){
            $this->id = $postArray['id'];
        }

        if(array_key_exists("UserID",$postArray)){
            $this->user_id = $postArray['UserID'];
        }

        if(array_key_exists("UserName",$postArray)){
            $this->user_name = $postArray['UserName'];
        }

        if(array_key_exists("Date_of_birth",$postArray)){
            $this->date_of_birth = $postArray['Date_of_birth'];
        }
    }//end of setData method

    public function store(){

        $sqlQuery = "INSERT INTO `birthday` (`user_id`,`user_name`,`date_of_birth`) VALUES (?,?,?); ";

        $dataArray = [$this->user_id,$this->user_name,$this->date_of_birth];

        $sth = $this->dbh->prepare($sqlQuery);

        $status = $sth->execute($dataArray);



        if($status) Message::message("Database has been inserted successfully!<br>") ;

        else Message::message("Failed to insert data!<br>");
    }//end of store method

    public function index(){

        $sqlQuery = "SELECT * FROM birthday WHERE is_trashed='NO'";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;

    }//end of index method

    public function view(){

        $sqlQuery = "Select * from birthday where id=".$this->id;


        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $oneData =  $sth->fetch();

        return $oneData;
    }//end of view function


    public function update(){

        $sqlQuery  = "UPDATE birthday SET user_id=?, user_name = ?, date_of_birth=? WHERE id=".$this->id;



        $dataArray = [$this->user_id,$this->user_name,$this->date_of_birth];



        $sth = $this->dbh->prepare($sqlQuery);

        $status =  $sth->execute($dataArray);

        if($status)
            Message::setMessage("Success! Data has been updated successfully. <br>");
        else
            Message::setMessage("Failed! Data has not been updated. <br>");


    }//end of update function


    public function trash(){

        $sqlQuery = "UPDATE birthday SET is_trashed=NOW() WHERE id=".$this->id;

        $status = $this->dbh->exec($sqlQuery);

        if($status) Message::setMessage("Success!Data has been trashed<br>");

        else Message::setMessage("Failed!Data has not been trashed<br>");


    }//end of trash function

    public function trashed(){

        $sqlQuery = "SELECT * FROM birthday WHERE is_trashed<>'NO'";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    }//end of trashed function


    public function recover(){


        $sqlQuery = "UPDATE birthday SET is_trashed='NO' WHERE id=".$this->id;

        $status = $this->dbh->exec($sqlQuery);

        if($status)
            Message::setMessage("Success! Data has been recovered successfully. <br>");
        else
            Message::setMessage("Failed! Data has not been recovered. <br>");



    }// end of recover function

    public function delete(){


        $sqlQuery = "DELETE FROM birthday WHERE id=".$this->id;

        $status = $this->dbh->exec($sqlQuery);

        if($status)
            Message::setMessage("Success! Data has been deleted successfully. <br>");
        else
            Message::setMessage("Failed! Data has not been deleted. <br>");



    }// end of delete function

    public function trashMultiple($selectedIDs){

        if( count($selectedIDs) == 0) {
            Message::message("Empty Selection! Please Select Some Record(s).");
            return;
        }

        $status = true;
        foreach ($selectedIDs as $id){

            $sqlQuery = "UPDATE birthday SET is_trashed=NOW() WHERE id=$id"  ;

            if ( ! $this->dbh-> exec($sqlQuery) )
                $status = false;
        }


        if($status)
            Message::message("Success! All Seleted Data Has Been Trashed");
        else
            Message::message("Failed! All Seleted Data Has Not Been Trashed");

    }// end of trashMultiple() Method





    public function recoverMultiple($selectedIDs){

        if( count($selectedIDs) == 0) {
            Message::message("Empty Selection! Please Select Some Record(s).");
            return;
        }

        $status = true;
        foreach ($selectedIDs as $id){

            $sqlQuery = "UPDATE birthday SET is_trashed='NO' WHERE id=$id"  ;

            if ( ! $this->dbh-> exec($sqlQuery) )
                $status = false;
        }


        if($status)
            Message::message("Success! All Seleted Data Has Been Recovered");
        else
            Message::message("Failed! All Seleted Data Has Not Been Recovered");

    }// end of recoverMultiple() Method



    public function deleteMultiple($selectedIDs){

        if( count($selectedIDs) == 0) {
            Message::message("Empty Selection! Please Select Some Record(s).");
            return;
        }

        $status = true;
        foreach ($selectedIDs as $id){

            $sqlQuery = "DELETE FROM birthday WHERE id=$id";

            if ( ! $this->dbh-> exec($sqlQuery) )
                $status = false;
        }


        if($status)
            Message::message("Success! All Seleted Data Has Been Deleted");
        else
            Message::message("Failed! All Seleted Data Has Not Been Deleted");

    }// end of deleteMultiple() Method

    public function searchIndex($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byDOB']) )  $sql = "SELECT * FROM `birthday` WHERE `is_trashed` ='No' AND (`user_name` LIKE '%".$requestArray['search']."%' OR `date_of_birth` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byDOB']) ) $sql = "SELECT * FROM `birthday` WHERE `is_trashed` ='No' AND `user_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byDOB']) )  $sql = "SELECT * FROM `birthday` WHERE `is_trashed` ='No' AND `date_of_birth` LIKE '%".$requestArray['search']."%'";

        $sth  = $this->dbh->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $someData = $sth->fetchAll();

        return $someData;

    }// end of search()


    public function searchIndexTrashed($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byDOB']) )  $sql = "SELECT * FROM `birthday` WHERE `is_trashed` <>'No' AND (`user_name` LIKE '%".$requestArray['search']."%' OR `date_of_birth` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byDOB']) ) $sql = "SELECT * FROM `birthday` WHERE `is_trashed` <>'No' AND `user_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byDOB']) )  $sql = "SELECT * FROM `birthday` WHERE `is_trashed` <>'No' AND `date_of_birth` LIKE '%".$requestArray['search']."%'";

        $sth  = $this->dbh->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $someData = $sth->fetchAll();

        return $someData;

    }// end of search()

    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);
        if($start<0) $start = 0;
        $sql = "SELECT * from birthday  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $STH = $this->dbh->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }

    public function trashedPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);
        if($start<0) $start = 0;
        $sql = "SELECT * from birthday  WHERE is_trashed <> 'No' LIMIT $start,$itemsPerPage";



        $STH = $this->dbh->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;




    }




    public function getAllKeywordsIndexTrashed()
    {
        $_allKeywords = array();
        $wordsArr = array();

        $allData = $this->trashed();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->user_name);
        }




        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->user_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $wordArr = explode(" ", $eachString);

            foreach ($wordArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->trashed();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->date_of_birth);
        }
        $allData = $this->trashed();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->date_of_birth);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $wordArr = explode(" ", $eachString);

            foreach ($wordArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords




    public function getAllKeywordsIndex()
    {
        $_allKeywords = array();
        $wordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->user_name);
        }




        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->user_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $wordArr = explode(" ", $eachString);

            foreach ($wordArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->date_of_birth);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->date_of_birth);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $wordArr = explode(" ", $eachString);

            foreach ($wordArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords


public function viewSortData2(){

        $sql = "SELECT * FROM birthday WHERE is_trashed='NO' ORDER BY MONTH(birthday),DAY(birthday),name";
        $sth = $this->dbh->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $temp = $sth->fetchAll();


        $storeSortedData = array();
        foreach ($temp as $sortArray) {
            $date = "" . $sortArray->date_of_birth;
            $d = date_parse_from_format("Y-m-d", $date);
            if (($d["month"] >= date("m")) && $d["day"] >= date("d")) {
                $storeSortedData[] = $sortArray;

            }

            else $notNextBirthday[] = $sortArray->id;
        }

        $storeSortedData = array_merge($storeSortedData,$notNextBirthday);

        return $storeSortedData;
        }



}//end of birthday class