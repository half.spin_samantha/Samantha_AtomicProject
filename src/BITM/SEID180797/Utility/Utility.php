<?php
/**
 * Created by PhpStorm.
 * User: saman
 * Date: 10/2/2017
 * Time: 9:54 AM
 */

namespace App\Utility;


class Utility
{
    public static function redirect($url){

        header("Location:$url");
    }

    public static function d($myVar)
    {
        echo "<pre>";
        var_dump($myVar);
        echo "<pre>";

    }

    public static function dd($myVar)
    {
        echo "<pre>";
        var_dump($myVar);
        echo "<pre>";
        die;

    }


}