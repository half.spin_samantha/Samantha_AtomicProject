<?php

require_once ("../../../vendor/autoload.php");

if(!isset($_SESSION)) session_start();

use App\Message\Message;

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <title>Atomic Project</title>

    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css">
    <script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
    <script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->


</head>


<body>

<body background="../../../resources/images/atomic_project.jpg">

<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center" >
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>

<div style="text-align: center;font-size: xx-large;font-family: 'Arial Black';color:#2098d1;background: lightsteelblue;padding-top: 30px;">
    <b>ATOMIC PROJECT</b>
    <br>

</div>


<nav class="navbar" style="font-family: 'Arial Black'; background: lightsteelblue" >
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a class="hvr-underline-from-center" href="../index.html">Home</a></li>
                <li><a class="hvr-underline-from-center" href="../BookTitle/index.php">Book Title</a></li>
                <li><a class="hvr-underline-from-center" href="../Birthday/index.php">Birthday</a></li>
                <li><a class="hvr-underline-from-center" href="../City/index.php">City</a></li>
                <li><a class="hvr-underline-from-center" href="../Email/index.php">Email</a></li>
                <li><a class="hvr-underline-from-center" href="../Gender/index.php">Gender</a></li>
                <li><a class="hvr-underline-from-center" href="../Hobbies/index.php">Hobbies</a></li>
                <li><a class="hvr-underline-from-center" href="../ProfilePicture/index.php">Profile Picture</a></li>
                <li><a class="hvr-underline-from-center" href="../SummaryOfOrganization/index.php">Summary of organization</a></li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="navbar container">
    <a href='create.php' class='btn btn-lg bg-success'>Create</a>
    <a href='index.php' class='btn btn-lg bg-danger'>Active List</a>
    <a href='trashed.php' class='btn btn-lg bg-danger'>Trashed List</a>
</div>


<div class="container bg-primary" style="margin-top: 100px">

    <h1 style="text-align: center"> Add New Birthdate </h1>

    <div class="col-md-2"> </div>


    <div class="col-md-8" style="margin-top: 50px; margin-bottom: 50px">
    <form action="store.php" method="post">

        <fieldset>
            <h2 style="text-align: center"><b> Birthday - Add Form </b></h2>

        <div class="form-group">
            <label for="UserID">User ID </label>
            <input type="text" class="form-control" name="UserID" placeholder="Enter the user id here....">
        </div>

        <div class="form-group">
            <label for="UserName">User Name: </label>
            <input type="text"  class="form-control" name="UserName" placeholder="Enter the user name here....">
        </div>

        <div class="form-group">
            <label for="Date_of_birth">Date Of Birth: </label>
            <input type="date"  class="form-control" name="Date_of_birth" placeholder="DD/MM/YY">
        </div>

        <button type="submit" class="btn btn-danger">Submit</button>

        </fieldset>
    </form>

</div>

    <div class="col-md-2" > </div>

</div>
    <script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

<script>
    $(function($){

        $("#message").fadeOut(600);
        $("#message").fadeIn(600);
        $("#message").fadeOut(600);
        $("#message").fadeOut(600);
        $("#message").fadeIn(600);
        $("#message").fadeOut(600);


    });


</script>


</body>
</html>
