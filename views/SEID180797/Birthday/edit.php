<?php

    require_once ("../../../vendor/autoload.php");

    if(!isset($_SESSION)) session_start();

    use App\Message\Message;
    use App\Birthday\Birthday;

    $obj = new Birthday();
    $obj->setData($_GET);
    $oneData = $obj->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <title>Create</title>

    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>


</head>


<body>


<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center" >
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>

<div class="container">
    <h1>Edit form<br></h1>
    <h2>Enter the User Birthdate</h2>

    <form action="update.php" method="post">


        <div class="form-group">
            <label for="UserID">User ID </label>
            <input type="text" class="form-control" name="UserID" placeholder="Enter the user id here....">
        </div>

        <div class="form-group">
            <label for="UserName">User Name: </label>
            <input type="text"  class="form-control" name="UserName" placeholder="Enter the user name here....">
        </div>

        <div class="form-group">
            <label for="Date_of_birth">Date Of Birth: </label>
            <input type="date"  class="form-control" name="Date_of_birth" placeholder="DD/MM/YY">
        </div>


        <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $oneData->id ?>">

        <button type="submit" class="btn btn-danger">Update</button>


    </form>
</div>

<<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>


<script>
    $(function($){

        $("#message").fadeOut(600);
        $("#message").fadeIn(600);
        $("#message").fadeOut(600);
        $("#message").fadeOut(600);
        $("#message").fadeIn(600);
        $("#message").fadeOut(600);


    });


</script>

</body>
</html>
