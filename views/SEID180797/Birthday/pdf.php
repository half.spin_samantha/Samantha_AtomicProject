<?php
include_once ('../../../vendor/autoload.php');
use App\Birthday\Birthday;

$obj= new Birthday();
 $allData=$obj->index();
 //var_dump($allData);
$trs="";
$sl=0;

    foreach($allData as $oneData) {
        $id =  $oneData->id;
        $user_id = $oneData->user_id;
        $user_name = $oneData->user_name;
        $date_of_birth =$oneData->date_of_birth;

        $sl++;
        $trs .= "<tr>";
        $trs .= "<td width='50'> $sl</td>";
        $trs .= "<td width='50'> $user_id </td>";
        $trs .= "<td width='250'> $user_name </td>";
        $trs .= "<td width='250'> $date_of_birth </td>";

        $trs .= "</tr>";
    }

$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >ID</th>
                    <th align='left' >Book Name</th>
                    <th align='left' >Author Name</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>


BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/src/Mpdf.php');
//Create an instance of the class:

$mpdf = new \Mpdf\Mpdf();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('Birthday.pdf', 'D');