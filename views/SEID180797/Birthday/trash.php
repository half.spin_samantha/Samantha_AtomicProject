<?php

require_once ("../../../vendor/autoload.php");

if(!isset($_SESSION)) session_start();

use App\Message\Message;
use App\Birthday\Birthday;
use App\Utility\Utility;

$obj = new Birthday();

$obj->setData($_GET);

$obj->trash();

Utility :: redirect("index.php");

?>