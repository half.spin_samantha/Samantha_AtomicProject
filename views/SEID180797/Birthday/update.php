<?php

require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
$obj = new \App\Birthday\Birthday();

$obj->setData($_POST);

$obj->update();

Utility::redirect('index.php?Page=1');